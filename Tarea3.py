print("UNIVERSIDAD NACIONAL DE LOJA")
print("Pedagogía de la Informática")
print("    Autora: Susana Tene")
print("    Fecha: 6 de Julio del 2020")

print("Ejercicio: Cree un módulo python llamado GEOMETRÍA que marque los siguientes puntos:")

import math

print("A=(2,3)")
print("B=(5,5)")
print("C=(-3, -1)")
print("D=(0,0)")

class Punto:
    x = int
    y = int

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("El punto:A{} pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("El punto:b{} pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("El punto:C{} pertenece al tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("El punto:E {} pertenece al cuarto cuadrante".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        else:
            self.x==0 and self.y==0
            print("El punto:D{} se encuentra sobre punto de origen".format(self))

    def vector(self, p):
        print("El vector entre los puntos {} y {} es igual ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y))

    def distancia(self, p):
        d = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        print("La distancia entre los puntos {} y {} es igual {}".format(self, p, d))

    def distancia_centro(self, p):
        d = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        return d

    def lejano_centro(self, a,b,c):
        if a>b and a>c:
            print("El punto A, es el mas lejano del punto centro")
        elif a<b and b>c:
            print("El punto B, es el mas lejano del punto centro")
        else:
            print("El punto C, ss el mas lejano del punto centro")

class Rectangulo:
    punto_inicial = Punto
    punto_final = Punto

    def __init__(self, punto_inicial=Punto(), punto_final=Punto()):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

        self.Base = abs(self.punto_final.x - self.punto_inicial.x)
        self.Altura = abs(self.punto_final.y - self.punto_inicial.y)
        self.Area = self.Base * self.Altura

    def base(self):
        print("La base del rectángulo es {}".format(self.Base))

    def altura(self):
        print("La altura del rectángulo es {}".format(self.Altura))

    def area(self):
        print("El área del rectángulo es {}".format(self.Area))

A = Punto(2,3)
B = Punto(5,5)
C = Punto(-3, -1)
D = Punto(0,0)

a = A.distancia_centro(D)
b = B.distancia_centro(D)
c = C.distancia_centro(D)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.lejano_centro(a,b,c)

T = Rectangulo(A, B)
T.base()
T.altura()
T.area()